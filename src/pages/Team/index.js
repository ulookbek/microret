import React from 'react';

const Team = () => {
    return (
        <>
            <div id="team" className="header">
                <div className={"header-text"}>
                    <h1 className={'text-uppercase'}>
                        TEAM <br/>
                        <p style={{textTransform: 'none'}}>
                            Bishkek-based Microret is a software development company specialized in startups in area of finance, retail, food supply, logistic, trading and consulting. Our exceptionally talented team of digital professionals can offer you innovative solutions to optimize your business and increase sales by using cutting edge technologies as Machine Learning, Data Science, Computer Vision, Big Data
                        </p>
                    </h1>
                </div>
                <div className={"header-empty"} style={{width: '50%'}}>
                </div>
            </div>
        </>
    );
};

export default Team;