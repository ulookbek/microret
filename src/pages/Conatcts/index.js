import React from 'react';
import WA from "../../assets/svg/icons/whatsapp.svg"
import World from "../../assets/svg/icons/world.svg"
import Email from "../../assets/svg/icons/email.svg"

const Contacts = () => {
    return (
        <div id="contacts" className="header">
            <div className={"header-text"}>
                <h1 className={'text-uppercase'}>
                    Our
                    <br/>
                    <span>contacts</span>
                    <div className={'info'}><img src={WA} style={{width: '20px'}} alt=""/><p>+996708342008</p></div>
                    <div className={'info'}><img src={World} style={{width: '20px'}} alt=""/><p>microret.com</p></div>
                    <div className={'info'}><img src={Email} style={{width: '20px'}} alt=""/><p>info@microret.com</p></div>
                </h1>

            </div>
            <div className={"header-empty"} style={{width: '40%'}}>
            </div>
        </div>
    );
};

export default Contacts;