import React from 'react';
import Stars from "../../components/Stars";

const Partners = () => {
    return (
        <>
            <Stars/>
            <div id="partners" className="header">
                <div className={"header-text"}>
                    <h1 className={'text-uppercase'}>
                        PARTNERS
                        <br/>
                        <span>Our partners</span></h1>
                </div>
                <div className={"header-empty"} style={{width: '50%'}}>
                </div>
            </div>
        </>

    );
};

export default Partners;