import React from 'react';

const Home = () => {

    return (
        <>
            <div id="home" className="header">
                <div className={"header-text"}>
                    <h1 className={'text-uppercase'}>
                        intelligent <br/>
                        <span>Business automation</span>
                        <h2 id={"uluk"}/>
                        <p>Modern Software Development. High load systems development with using AI</p>
                    </h1>

                    <a href="#contacts">
                        Contact
                    </a>
                </div>
                <div className={"header-empty"} style={{width: '50%'}}>

                </div>
            </div>
        </>
    );
};

export default Home;


