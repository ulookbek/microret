import React from 'react';
import Stars from "../../components/Stars";

const Projects = () => {
    return (
        <>
            <Stars/>
            <div id="projects" className="header">
                <div className={"header-text"}>
                    <h1 className={'text-uppercase'}>
                        Projects:<br/>
                        <p>
                            -  Payment System <br/>
                            -  E-Wallet<br/>
                            -  Bank Loan Scoring System with using Machine Learning<br/>
                            -  Trading robot<br/>
                            -  Automated Voice assistant<br/>
                        </p></h1>
                </div>
                <div className={"header-empty"} style={{width: '50%'}}>
                </div>
            </div>
        </>
    );
};

export default Projects;