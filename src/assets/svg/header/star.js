import React from "react";

function Icon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="130.697"
            height="3.149"
            viewBox="0 0 130.697 3.149"
        >
            <defs>
                <linearGradient
                    id="linear-gradient"
                    x1="4.116"
                    x2="5.116"
                    y1="249.399"
                    y2="249.399"
                    gradientUnits="objectBoundingBox"
                >
                    <stop offset="0" stopColor="#00e7fa"></stop>
                    <stop offset="0.187" stopColor="black" stopOpacity="0.753"></stop>
                    <stop offset="0.407" stopColor="#06869a" stopOpacity="0.49"></stop>
                    <stop offset="0.607" stopColor="#00e7fa" stopOpacity="0.278"></stop>
                    <stop offset="0.778" stopColor="#00e7fa" stopOpacity="0.125"></stop>
                    <stop offset="1" stopColor="#00e7fa" stopOpacity="0.035"></stop>
                    <stop offset="1" stopColor="#00e7fa" stopOpacity="0"></stop>
                </linearGradient>
            </defs>
            <path
                fill="url(#linear-gradient)"
                d="M593.433 167.477H465.884a1.574 1.574 0 00-1.574 1.574 1.575 1.575 0 001.574 1.575h127.549a1.575 1.575 0 001.574-1.575 1.574 1.574 0 00-1.574-1.574z"
                transform="translate(-464.31 -167.477)"
            ></path>
        </svg>
    );
}

export default Icon;
