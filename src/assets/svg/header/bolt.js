import React from "react";

function Icon({width}) {
    return (
        <div>
            <svg
                // style={{backgroundColor: '#fff'}}
                xmlns="http://www.w3.org/2000/svg"
                width={width}
                height="150.934"
                viewBox="0 0 52.372 51.934"
            >
                <defs>
                    <radialGradient
                        id="radial-gradient"
                        cx="0.5"
                        cy="0.5"
                        r="0.5"
                        gradientUnits="objectBoundingBox"
                    >
                        <stop offset="0"></stop>
                        <stop offset="0.029" stopColor="#00e0f8"></stop>
                        <stop offset="1" stopColor="#a4b2f5"></stop>
                    </radialGradient>
                </defs>
                <g data-name="Сгруппировать 34" transform="translate(.5 .5)">
                    <g
                        data-name="Сгруппировать 33"
                        transform="translate(6.822 6.764)"
                        // style={{ mixBlendMode: "color-dodge", isolation: "isolate" }}
                    >
                        <ellipse
                            cx="18.863"
                            cy="18.703"
                            fill="none"
                            stroke="#0026ae"
                            strokeDasharray="16.612 4.886"
                            strokeMiterlimit="10"
                            strokeWidth="3"
                            data-name="Эллипс 76"
                            rx="18.863"
                            ry="18.703"
                        ></ellipse>
                    </g>
                    <path
                        fill="none"
                        stroke="#00e0f8"
                        strokeWidth="1"
                        d="M13.147 22.532a12.877 12.877 0 0125.076 5.87 12.877 12.877 0 01-25.076-5.87z"
                        data-name="Контур 439"
                        style={{ mixBlendMode: "color-dodge", isolation: "isolate" }}
                    ></path>
                    <ellipse
                        cx="25.686"
                        cy="25.467"
                        fill="none"
                        stroke="#00e0f8"
                        strokeMiterlimit="10"
                        strokeWidth="1"
                        data-name="Эллипс 78"
                        rx="25.686"
                        ry="25.467"
                        style={{ mixBlendMode: "color-dodge", isolation: "isolate" }}
                    ></ellipse>
                    <ellipse
                        cx="6.764"
                        cy="6.706"
                        fill="url(#radial-gradient)"
                        data-name="Эллипс 79"
                        rx="6.764"
                        ry="6.706"
                        transform="translate(18.922 18.761)"
                        style={{ mixBlendMode: "color-dodge", isolation: "isolate" }}
                    ></ellipse>
                </g>
            </svg>
        </div>
    );
}

export default Icon;
