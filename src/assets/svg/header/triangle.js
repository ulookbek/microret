import React from "react";

function Triangle() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="63.111"
            height="29.399"
            viewBox="0 0 63.111 29.399"
        >
            <defs>
                <linearGradient
                    id="linear-gradient"
                    x1="0.067"
                    x2="3.525"
                    y1="0.5"
                    y2="0.5"
                    gradientUnits="objectBoundingBox"
                >
                    <stop offset="0" stopColor="#0026ae"></stop>
                    <stop offset="1"></stop>
                </linearGradient>
            </defs>
            <path
                fill="url(#linear-gradient)"
                d="M525.339 135.385a2.017 2.017 0 00.927-.232l15.223 22.108a1.987 1.987 0 00-.7 1.5 2.024 2.024 0 004.048 0 1.967 1.967 0 00-.118-.648l37.981-18.251a2 2 0 10-.278-1.507l-55.067-5.035a2.021 2.021 0 10-2.018 2.064zm1.976-1.584l55.068 5.035a1.952 1.952 0 00.112.591l-37.977 18.253a2 2 0 00-2.627-.69l-15.223-22.109a1.989 1.989 0 00.648-1.081z"
                data-name="Контур 395"
                transform="translate(-523.315 -131.372)"
            ></path>
        </svg>
    );
}

export default Triangle;
