import React from 'react';

const Stars = () => {
    return (
        <div id="particle-container">
            {
                (function() {
                    const arr = [];
                    for(let i = 0; i <= 200; i++) {
                        arr.push(<div className="particle"/>)
                    }
                    return arr;
                })()
            }
        </div>
    );
};

export default Stars;