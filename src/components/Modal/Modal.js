import React from 'react';

const Modal = () => {
    return (
        <div className="hystmodal" id="myModal" aria-hidden="true">
            <div className="hystmodal__wrap">
                <div className="hystmodal__window" role="dialog" aria-modal="true">
                    <button data-hystclose className="hystmodal__close">Close</button>
                    <h1>Заголовок модального окна</h1>
                    <p>Текст модального окна ...</p>
                    <img src="img/photo.jpg" alt="Изображение" width="400"/>
                    <p>Ещё текст модального окна ...</p>
                </div>
            </div>
        </div>
    );
};

export default Modal;