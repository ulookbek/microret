import React from 'react';
import logo from "../../assets/svg/logo_light.svg"
import burger from "../../assets/svg/icons/menu.svg"
import Modal from 'react-modal';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        backgroundColor       : 'rgba(36,65,119,0.7)',

    },
};

Modal.setAppElement('#root')
Modal.defaultStyles.overlay.backgroundColor = 'rgba(36,65,119,0.7)';



const Navigation = () => {
    const [modalIsOpen,setIsOpen] = React.useState(false);
    function openModal() {
        setIsOpen(true);
    }

    function closeModal(){
        setIsOpen(false);
    }
    return (
        <>
            <nav className={"navigation pt-7 d-flex justify-space-between"}>
                <img className={"logo"} src={logo} alt="microret logo"/>
                <ul className={"d-flex list-style-none text-uppercase"}>
                    <li><a href="#home">home</a></li>
                    <li><a href="#team">team</a></li>
                    <li><a href="#projects">projects</a></li>
                    {/*<li><a href="#partners">partners</a></li>*/}
                    <li><a href="#contacts">contact</a></li>
                </ul>
                <button type="button" onClick={openModal} className={"burger"} style={{background: `url(${burger}) 50% no-repeat`}}>
                </button>
            </nav>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
            >
                <ul className={"in-modal-ul d-flex fd-column text-center list-style-none text-uppercase"}>
                    <li><a href="#home">home</a></li>
                    <li><a href="#team">team</a></li>
                    <li><a href="#projects">projects</a></li>
                    {/*<li><a href="#partners">partners</a></li>*/}
                    <li><a href="#contacts">contact</a></li>
                </ul>
            </Modal>
        </>
    );
};

export default Navigation;