import Home from "./pages/Home";
import Navigation from "./components/navigation";
import Bolt from "./assets/svg/header/bolt";
import Triangle from "./assets/svg/header/triangle";
import bgHeader from "./assets/img/header/bg-of-header.svg";
import Projects from "./pages/Projects";
import Team from "./pages/Team";
import Partners from "./pages/Partners";
import Stars from "./components/Stars";
import React from "react";
import Contacts from "./pages/Conatcts";

function App() {

    const text = [
        'MACHINE LEARNING\n',
        'DATA SCIENCE\n',
        'artificial intelligence\n',
    ];

    let line = 0;
    let count = 0;
    let result = '';

    function typeLine() {
        let interval = setTimeout(
            () => {
                result += text[line][count]
                const uluk = document.getElementById('uluk')
                uluk.innerHTML = result + '|';

                count++;
                if(count >= text[line].length) {
                    count = 0;
                    line++;
                    uluk.innerHTML = '|'
                    if(line !== text.length) {
                        result = ''
                    }
                    if(line === text.length) {
                        clearTimeout(interval);
                        uluk.innerHTML = result;
                        typeLine()
                        line = 0;
                        count = 0;
                        result = ''
                        return true;
                    }
                }
                typeLine();
            }, 200)
    }

    typeLine();

    const styles = {
        background: `url(${bgHeader}) 50% no-repeat`,
        backgroundSize: 'cover',
        backgroundAttachment: 'fixed',
    }

    return (
        <div className="home">
            <Navigation/>
            <Stars/>
            <div className={'bolt-1'}><Bolt width={'150'}/></div>
            <div className={'bolt-2'}><Bolt width={'60'}/></div>
            <div className={'bolt-3'}><Triangle/></div>
            <main style={styles}>
                <Home/>
                <Projects/>
                <Team/>
                <Partners/>
                <Contacts/>
            </main>
        </div>
    );
}

export default App;
